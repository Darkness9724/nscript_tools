Unofficial mirror of http://nscripter.insani.org/sdk.html

NScripter SDK by Naoki Takahashi. Win32 only.\
Contents:
* nscr.exe - mainline NScripter runtime binary. Included only for completeness' sake. Do not use in your translation project, as it does not support 1-byte characters.
* nscmake.exe - takes your script file (0.txt) and converts it into compiled bytecode for the runtime binary (nscript.dat).
* nsaarc.exe - archives your resources into arc*.nsa format for distribution.
* bw2aconv.exe - simple alpha mask generation program.

NSDec.exe by Liquid Systems and nsdec.cpp by ONScripter-RU - script decompression from any given nscript.dat.

nscmake.cpp by ONScripter-RU - script compression to nscript.dat.

NSAOut.exe and NSAOut.cpp by alamone - extracts the resources from arc*.nsa.

SARDec.exe by Studio OGA - extracts the resources from arc*.sar.
